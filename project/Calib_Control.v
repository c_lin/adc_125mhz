module Calib_Control(
	clk,
	ENA,
	pulse_ena,
	trig
);

input wire clk;
input wire ENA;

output reg pulse_ena;
output reg trig;

reg [9:0] control;
reg [1:0] pipeline;
reg ena = 1'b0;

always @(posedge clk)
begin
	
	pipeline[1] <= pipeline[0];
	pipeline[0] <= ENA;
	
	if( pipeline[0] == 1'b1 && pipeline[1] == 1'b0 ) 
	begin
		ena = 1'b1;
		control = 10'b0;
	end
	
	if(ena)
	begin
	
		pulse_ena = ( control == 44 )? 1'b1 : 1'b0;
		
		if( control >= 201 && control <= 203 ) 
		begin
			trig = 1'b1;
		end
		else if( control >= 512 && control <= 512 ) 
		begin
			trig = 1'b1;
		end
		else if( control == 513 )
		begin
			trig = 1'b0;
			control = 10'b0;
			ena = 1'b0;
		end
		else
		begin
			trig = 1'b0;
		end
		
		control = control + 1'b1;
		
	end
	

end

endmodule
