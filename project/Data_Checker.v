module Data_Checker (

	input clk,
	input rst,
	input live,
	input load_packet,
	input [13:0] wdata,
	input get_packet,
	input [13:0] rdata,
	output reg [13:0] data_in,
	output reg [13:0] data_out,
	output reg dv
);

reg [13:0] ram_w [255:0];
reg [13:0] ram_r [255:0];
reg [7:0] local_waddr = 0;
reg [7:0] local_raddr = 0;
reg [7:0] nevt_out = 0;

reg [6:0] load_packet_pipe;
reg [6:0] get_packet_pipe;


always @ (posedge clk)
begin


	if(rst | ~live)
	begin
		local_waddr = 8'b0;
		local_raddr = 8'b0;
		nevt_out = 8'b0;
		dv = 0;
		data_in = 13'b0;
		data_out = 13'b0;
	end
	
	
	load_packet_pipe = load_packet_pipe << 1;
	load_packet_pipe[0] = load_packet;
	
	get_packet_pipe = get_packet_pipe << 1;
	get_packet_pipe[0] = get_packet;

	if(load_packet_pipe[2]==1'b1)
	begin
		ram_w[local_waddr] = wdata;
		local_waddr = local_waddr + 1'b1;
	end
	
	if(get_packet_pipe[5]==1'b1)
	begin
		ram_r[local_raddr] = rdata;
		local_raddr = local_raddr + 1'b1;
		nevt_out = nevt_out + 1'b1;
	end	
	
	if(nevt_out>0)
	begin
		data_in = ram_w[local_raddr-1'b1];
		data_out = ram_r[local_raddr-1'b1];
		dv = 1'b1;
		nevt_out = nevt_out - 1'b1;
	end
	else
	begin
		data_in = 13'b0;
		data_out = 13'b0;
		dv = 1'b0;
	end
end
endmodule