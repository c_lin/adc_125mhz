module CB_decision(
	clk,
	pt,
	ph,
	ph_2sample,
	thres,
	thres_2nd,
	tmin,
	tmax,
	mask,
	ena_simu,
	cbit_simu,
	cbit_reg,
	cbit_reg_2nd,
	pt_reg,
	ph_reg,
	ph_fixed
);

input wire clk;

input wire [5:0] pt;
input wire [13:0] ph;
input wire [13:0] ph_2sample;
input wire [13:0] thres;
input wire [13:0] thres_2nd;
input wire [5:0] tmin;
input wire [5:0] tmax;
input wire mask;
input wire ena_simu;
input wire cbit_simu;


output reg cbit_reg;
output reg cbit_reg_2nd;
output reg [5:0]  pt_reg;
output reg [13:0] ph_reg;
output reg [13:0] ph_fixed;



always @(posedge clk)
begin

	cbit_reg = (ph >= thres && pt >= tmin && pt <= tmax && ~mask)? 1'b1:1'b0;
	cbit_reg_2nd = (ph >= thres_2nd && pt >= tmin && pt <= tmax && ~mask)? 1'b1:1'b0;

	ph_fixed = (pt >= tmin && pt <= tmax && ~mask)? ph_2sample : 14'b0;
	
	

	ph_reg = ph;
	pt_reg = pt;
	
	cbit_reg = (ena_simu==1'b1)? cbit_simu:cbit_reg;
	cbit_reg_2nd = (ena_simu==1'b1)? cbit_simu:cbit_reg_2nd;
	

end

endmodule