## Generated SDC file "ADC_125mhz.sdc"

## Copyright (C) 1991-2012 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 12.0 Build 178 05/31/2012 SJ Full Version"

## DATE    "Wed Dec 14 10:01:19 2016"

##
## DEVICE  "EP2S60F1020C5"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {master_clock1} -period 8.000 -waveform { 0.000 4.000 } [get_ports {master_clock1}]
create_clock -name {clk_125mhz} -period 8.000 -waveform { 0.000 4.000 } [get_ports {clk_125mhz}]
create_clock -name {adc_clk0} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[0]}]
create_clock -name {adc_clk1} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[1]}]
create_clock -name {adc_clk2} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[2]}]
create_clock -name {adc_clk3} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[3]}]
create_clock -name {adc_clk4} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[4]}]
create_clock -name {adc_clk5} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[5]}]
create_clock -name {adc_clk6} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[6]}]
create_clock -name {adc_clk7} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[7]}]
create_clock -name {adc_clk8} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[8]}]
create_clock -name {adc_clk9} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[9]}]
create_clock -name {adc_clk10} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[10]}]
create_clock -name {adc_clk11} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[11]}]
create_clock -name {adc_clk12} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[12]}]
create_clock -name {adc_clk13} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[13]}]
create_clock -name {adc_clk14} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[14]}]
create_clock -name {adc_clk15} -period 8.000 -waveform { 0.000 4.000 } [get_ports {adc_clk[15]}]
create_clock -name {L1_rx_clk} -period 8.000 -waveform { 0.000 4.000 } [get_ports {L1_rx_clk}]
create_clock -name {L2_rx_clk} -period 8.000 -waveform { 0.000 4.000 } [get_ports {L2_rx_clk}]
create_clock -name {vme_virtual_clock} -period 80.000 -waveform { 0.000 40.000 } 
create_clock -name {vme_ds0} -period 80.000 -waveform { 64.000 88.000 } [get_ports {vme_ds0}]
create_clock -name {vme_ds1} -period 80.000 -waveform { 64.000 88.000 } [get_ports {vme_ds1}]
create_clock -name {vme_as} -period 80.000 -waveform { 64.000 88.000 } [get_ports {vme_as}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {pll_125mhz} -source [get_ports {clk_125mhz}] -duty_cycle 50.000 -multiply_by 1 -master_clock {clk_125mhz} [get_nets {inst12|altpll_component|_clk1}] 
create_generated_clock -name {pll_125mhz_inv} -source [get_ports {clk_125mhz}] -duty_cycle 50.000 -multiply_by 1 -phase 210.000 -master_clock {clk_125mhz} [get_nets {inst12|altpll_component|_clk3}] 
create_generated_clock -name {pll_156mhz} -source [get_ports {clk_125mhz}] -master_clock {clk_125mhz} [get_nets {inst11|altpll_component|_clk0}] 
create_generated_clock -name {pll_156mhz_inv} -source [get_ports {clk_125mhz}] -phase 213.750 -master_clock {clk_125mhz} [get_nets {inst11|altpll_component|_clk2}] 
create_generated_clock -name {sample_clock} -source [get_ports {clk_125mhz}] -duty_cycle 50.000 -multiply_by 1 -phase 72.000 -master_clock {clk_125mhz} [get_nets {inst9|altpll_component|_clk4}] 
create_generated_clock -name {samplingclock} -source [get_nets {inst9|altpll_component|_clk4}] -master_clock {sample_clock} [get_ports {samplingclock}] 
create_generated_clock -name {L1_clk} -source [get_nets {inst12|altpll_component|_clk3}] -master_clock {pll_125mhz_inv} [get_ports {L1_clk}] 
create_generated_clock -name {L2_clk} -source [get_nets {inst11|altpll_component|_clk2}] -master_clock {pll_156mhz_inv} [get_ports {L2_clk}] 
create_generated_clock -name {pll_vme_clk} -source [get_ports {clk_125mhz}] -duty_cycle 50.000 -multiply_by 1 -master_clock {clk_125mhz} [get_nets {inst24|altpll_component|_clk1}] 
create_generated_clock -name {energy_sum_clk} -source [get_pins {vme_ds0|combout}] -master_clock {vme_ds0} [get_registers {vme_interface_fast*inst111*}] 
create_generated_clock -name {data_buffer_clk} -source [get_pins {vme_ds0|combout}] -master_clock {vme_ds0} [get_registers {vme_interface_fast*inst134*}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {master_clock1}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {clk_125mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk0}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk1}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk2}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk3}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk4}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk5}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk6}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk7}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk8}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk9}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk10}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk11}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk12}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk13}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk14}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {adc_clk15}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_rx_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_rx_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_125mhz_inv}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_156mhz_inv}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {sample_clock}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {samplingclock}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L1_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {L2_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -rise_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_125mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_156mhz}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -rise_to [get_clocks {pll_vme_clk}]  0.200  
set_clock_uncertainty -fall_from [get_clocks {pll_vme_clk}] -fall_to [get_clocks {pll_vme_clk}]  0.200  


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {clk_125mhz}]  4.200 [get_ports {L1A}]
set_input_delay -add_delay -min -clock [get_clocks {clk_125mhz}]  3.800 [get_ports {L1A}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[0]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[0]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[1]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[1]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[2]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[2]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[3]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[3]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[4]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[4]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[5]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[5]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[6]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[6]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[7]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[7]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[8]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[8]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[9]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[9]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[10]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[10]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[11]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[11]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[12]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[12]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[13]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[13]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[14]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[14]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_data[15]}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_data[15]}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_enable}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_enable}]
set_input_delay -add_delay -max -clock [get_clocks {L1_rx_clk}]  4.200 [get_ports {L1_rx_error}]
set_input_delay -add_delay -min -clock [get_clocks {L1_rx_clk}]  3.800 [get_ports {L1_rx_error}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[0]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[0]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[1]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[1]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[2]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[2]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[3]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[3]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[4]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[4]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[5]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[5]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[6]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[6]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[7]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[7]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[8]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[8]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[9]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[9]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[10]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[10]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[11]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[11]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[12]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[12]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[13]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[13]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[14]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[14]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_data[15]}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_data[15]}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_enable}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_enable}]
set_input_delay -add_delay -max -clock [get_clocks {L2_rx_clk}]  4.200 [get_ports {L2_rx_error}]
set_input_delay -add_delay -min -clock [get_clocks {L2_rx_clk}]  3.800 [get_ports {L2_rx_error}]
set_input_delay -add_delay -max -clock [get_clocks {clk_125mhz}]  4.200 [get_ports {LIVE}]
set_input_delay -add_delay -min -clock [get_clocks {clk_125mhz}]  3.800 [get_ports {LIVE}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[0]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[0]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[1]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[1]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[2]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[2]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[3]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[3]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[4]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[4]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[5]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[5]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[6]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[6]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[7]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[7]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[8]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[8]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[9]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[9]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[10]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[10]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[11]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[11]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[12]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[12]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc[13]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc[13]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[14]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[14]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[15]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[15]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[16]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[16]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[17]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[17]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[18]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[18]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[19]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[19]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[20]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[20]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[21]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[21]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[22]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[22]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[23]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[23]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[24]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[24]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[25]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[25]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[26]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[26]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc[27]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc[27]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[28]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[28]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[29]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[29]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[30]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[30]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[31]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[31]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[32]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[32]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[33]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[33]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[34]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[34]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[35]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[35]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[36]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[36]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[37]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[37]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[38]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[38]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[39]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[39]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[40]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[40]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc[41]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc[41]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[42]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[42]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[43]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[43]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[44]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[44]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[45]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[45]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[46]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[46]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[47]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[47]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[48]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[48]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[49]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[49]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[50]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[50]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[51]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[51]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[52]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[52]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[53]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[53]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[54]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[54]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc[55]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc[55]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[56]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[56]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[57]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[57]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[58]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[58]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[59]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[59]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[60]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[60]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[61]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[61]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[62]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[62]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[63]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[63]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[64]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[64]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[65]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[65]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[66]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[66]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[67]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[67]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[68]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[68]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc[69]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc[69]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[70]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[70]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[71]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[71]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[72]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[72]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[73]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[73]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[74]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[74]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[75]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[75]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[76]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[76]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[77]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[77]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[78]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[78]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[79]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[79]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[80]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[80]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[81]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[81]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[82]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[82]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc[83]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc[83]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[84]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[84]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[85]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[85]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[86]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[86]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[87]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[87]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[88]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[88]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[89]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[89]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[90]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[90]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[91]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[91]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[92]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[92]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[93]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[93]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[94]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[94]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[95]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[95]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[96]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[96]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc[97]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc[97]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[98]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[98]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[99]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[99]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[100]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[100]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[101]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[101]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[102]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[102]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[103]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[103]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[104]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[104]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[105]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[105]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[106]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[106]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[107]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[107]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[108]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[108]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[109]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[109]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[110]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[110]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc[111]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc[111]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[112]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[112]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[113]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[113]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[114]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[114]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[115]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[115]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[116]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[116]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[117]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[117]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[118]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[118]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[119]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[119]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[120]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[120]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[121]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[121]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[122]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[122]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[123]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[123]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[124]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[124]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc[125]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc[125]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[126]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[126]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[127]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[127]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[128]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[128]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[129]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[129]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[130]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[130]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[131]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[131]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[132]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[132]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[133]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[133]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[134]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[134]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[135]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[135]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[136]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[136]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[137]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[137]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[138]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[138]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc[139]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc[139]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[140]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[140]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[141]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[141]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[142]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[142]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[143]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[143]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[144]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[144]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[145]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[145]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[146]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[146]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[147]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[147]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[148]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[148]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[149]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[149]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[150]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[150]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[151]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[151]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[152]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[152]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc[153]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc[153]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[154]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[154]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[155]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[155]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[156]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[156]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[157]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[157]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[158]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[158]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[159]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[159]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[160]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[160]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[161]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[161]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[162]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[162]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[163]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[163]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[164]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[164]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[165]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[165]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[166]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[166]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc[167]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc[167]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[168]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[168]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[169]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[169]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[170]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[170]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[171]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[171]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[172]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[172]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[173]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[173]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[174]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[174]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[175]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[175]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[176]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[176]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[177]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[177]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[178]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[178]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[179]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[179]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[180]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[180]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc[181]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc[181]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[182]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[182]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[183]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[183]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[184]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[184]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[185]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[185]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[186]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[186]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[187]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[187]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[188]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[188]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[189]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[189]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[190]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[190]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[191]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[191]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[192]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[192]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[193]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[193]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[194]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[194]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc[195]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc[195]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[196]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[196]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[197]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[197]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[198]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[198]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[199]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[199]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[200]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[200]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[201]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[201]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[202]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[202]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[203]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[203]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[204]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[204]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[205]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[205]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[206]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[206]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[207]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[207]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[208]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[208]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc[209]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc[209]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[210]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[210]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[211]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[211]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[212]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[212]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[213]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[213]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[214]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[214]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[215]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[215]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[216]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[216]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[217]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[217]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[218]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[218]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[219]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[219]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[220]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[220]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[221]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[221]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[222]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[222]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc[223]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc[223]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk0}]  4.200 [get_ports {adc_or[0]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk0}]  3.800 [get_ports {adc_or[0]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk1}]  4.200 [get_ports {adc_or[1]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk1}]  3.800 [get_ports {adc_or[1]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk2}]  4.200 [get_ports {adc_or[2]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk2}]  3.800 [get_ports {adc_or[2]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk3}]  4.200 [get_ports {adc_or[3]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk3}]  3.800 [get_ports {adc_or[3]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk4}]  4.200 [get_ports {adc_or[4]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk4}]  3.800 [get_ports {adc_or[4]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk5}]  4.200 [get_ports {adc_or[5]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk5}]  3.800 [get_ports {adc_or[5]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk6}]  4.200 [get_ports {adc_or[6]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk6}]  3.800 [get_ports {adc_or[6]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk7}]  4.200 [get_ports {adc_or[7]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk7}]  3.800 [get_ports {adc_or[7]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk8}]  4.200 [get_ports {adc_or[8]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk8}]  3.800 [get_ports {adc_or[8]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk9}]  4.200 [get_ports {adc_or[9]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk9}]  3.800 [get_ports {adc_or[9]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk10}]  4.200 [get_ports {adc_or[10]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk10}]  3.800 [get_ports {adc_or[10]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk11}]  4.200 [get_ports {adc_or[11]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk11}]  3.800 [get_ports {adc_or[11]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk12}]  4.200 [get_ports {adc_or[12]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk12}]  3.800 [get_ports {adc_or[12]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk13}]  4.200 [get_ports {adc_or[13]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk13}]  3.800 [get_ports {adc_or[13]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk14}]  4.200 [get_ports {adc_or[14]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk14}]  3.800 [get_ports {adc_or[14]}]
set_input_delay -add_delay -max -clock [get_clocks {adc_clk15}]  4.200 [get_ports {adc_or[15]}]
set_input_delay -add_delay -min -clock [get_clocks {adc_clk15}]  3.800 [get_ports {adc_or[15]}]
set_input_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  4.000 [get_ports {read_crc_error}]
set_input_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {read_crc_error}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[2]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[2]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[3]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[3]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[4]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[4]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[5]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[5]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[6]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[6]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[7]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[7]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[8]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[8]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[9]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[9]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[10]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[10]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[11]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[11]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[12]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[12]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[13]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[13]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[14]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[14]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[15]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[15]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[16]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[16]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[17]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[17]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[18]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[18]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[19]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[19]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[20]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[20]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[21]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[21]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[22]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[22]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[23]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[23]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[24]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[24]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[25]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[25]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[26]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[26]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[27]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[27]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[28]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[28]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[29]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[29]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[30]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[30]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_address[31]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_address[31]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[0]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[0]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[1]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[1]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[2]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[2]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[3]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[3]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[4]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[4]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_am[5]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_am[5]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[0]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[0]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[1]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[1]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[2]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[2]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[3]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[3]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[4]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[4]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[5]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[5]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[6]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[6]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[7]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[7]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[8]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[8]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[9]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[9]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[10]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[10]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[11]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[11]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[12]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[12]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[13]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[13]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[14]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[14]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[15]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[15]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[16]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[16]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[17]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[17]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[18]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[18]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[19]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[19]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[20]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[20]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[21]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[21]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[22]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[22]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[23]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[23]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[24]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[24]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[25]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[25]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[26]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[26]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[27]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[27]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[28]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[28]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[29]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[29]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[30]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[30]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_data[31]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_data[31]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ds0}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ds0}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ds1}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ds1}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ga[0]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ga[0]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ga[1]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ga[1]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ga[2]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ga[2]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ga[3]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ga[3]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_ga[4]}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_ga[4]}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_iackin}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_iackin}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_lword}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_lword}]
set_input_delay -add_delay -max -clock [get_clocks {vme_virtual_clock}]  5.000 [get_ports {vme_write}]
set_input_delay -add_delay -min -clock [get_clocks {vme_virtual_clock}]  2.000 [get_ports {vme_write}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {Board_error}]
set_output_delay -add_delay  -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {L1_chip_enable}]
set_output_delay -add_delay  -clock [get_clocks {L1_clk}]  0.000 [get_ports {L1_clk}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[0]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[0]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[1]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[1]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[2]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[2]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[3]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[3]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[4]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[4]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[5]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[5]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[6]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[6]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[7]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[7]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[8]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[8]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[9]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[9]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[10]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[10]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[11]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[11]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[12]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[12]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[13]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[13]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[14]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[14]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_data[15]}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_data[15]}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_enable}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_enable}]
set_output_delay -add_delay -max -clock [get_clocks {L1_clk}]  3.500 [get_ports {L1_error}]
set_output_delay -add_delay -min -clock [get_clocks {L1_clk}]  -3.500 [get_ports {L1_error}]
set_output_delay -add_delay  -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {L2_chip_enable}]
set_output_delay -add_delay  -clock [get_clocks {L2_clk}]  0.000 [get_ports {L2_clk}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[0]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[0]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[1]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[1]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[2]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[2]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[3]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[3]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[4]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[4]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[5]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[5]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[6]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[6]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[7]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[7]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[8]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[8]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[9]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[9]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[10]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[10]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[11]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[11]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[12]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[12]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[13]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[13]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[14]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[14]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_data[15]}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_data[15]}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_enable}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_enable}]
set_output_delay -add_delay -max -clock [get_clocks {L2_clk}]  3.500 [get_ports {L2_error}]
set_output_delay -add_delay -min -clock [get_clocks {L2_clk}]  -3.500 [get_ports {L2_error}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  4.000 [get_ports {lckrefn_l1}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {lckrefn_l1}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  4.000 [get_ports {lckrefn_l2}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {lckrefn_l2}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led11}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led11}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led12}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led12}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led21}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led21}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led22}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led22}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led23}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led23}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led31}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led31}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led32}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led32}]
set_output_delay -add_delay -max -clock [get_clocks {pll_125mhz}]  2.000 [get_ports {led33}]
set_output_delay -add_delay -min -clock [get_clocks {pll_125mhz}]  1.000 [get_ports {led33}]
set_output_delay -add_delay  -clock [get_clocks {samplingclock}]  0.000 [get_ports {samplingclock}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[0]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[0]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[1]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[1]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[2]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[2]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[3]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[3]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[4]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[4]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[5]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[5]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[6]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[6]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[7]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[7]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[8]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[8]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[9]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[9]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[10]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[10]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[11]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[11]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[12]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[12]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[13]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[13]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[14]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[14]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[15]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[15]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[16]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[16]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[17]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[17]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[18]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[18]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[19]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[19]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[20]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[20]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[21]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[21]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[22]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[22]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[23]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[23]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[24]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[24]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[25]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[25]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[26]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[26]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[27]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[27]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[28]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[28]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[29]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[29]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[30]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[30]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_data[31]}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_data[31]}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_dir}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_dir}]
set_output_delay -add_delay -max -clock [get_clocks {pll_vme_clk}]  2.000 [get_ports {vme_dtack}]
set_output_delay -add_delay -min -clock [get_clocks {pll_vme_clk}]  1.000 [get_ports {vme_dtack}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_te9:dffpipe19|dffe20a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_se9:dffpipe16|dffe17a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_id9:dffpipe18|dffe19a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_hd9:dffpipe15|dffe16a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_1f9:dffpipe22|dffe23a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_ue9:dffpipe14|dffe15a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_ed9:dffpipe15|dffe16a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_dd9:dffpipe12|dffe13a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_re9:dffpipe18|dffe19a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_qe9:dffpipe15|dffe16a*}]
set_false_path -from [get_keepers {*rdptr_g*}] -to [get_keepers {*ws_dgrp|dffpipe_jd9:dffpipe19|dffe20a*}]
set_false_path -from [get_keepers {*delayed_wrptr_g*}] -to [get_keepers {*rs_dgwp|dffpipe_gd9:dffpipe15|dffe16a*}]
set_false_path -from [get_registers *] -to [get_ports {led*}]
set_false_path -from [get_ports {vme*}] -to [get_ports {led*}]
set_false_path -from [get_registers {*adc_sync*inst1*lpm_ff*}] -to [get_registers {*display_driver*}]
set_false_path -from [get_clocks {clk_125mhz}] -to [get_ports {L1_chip_enable L2_chip_enable}]
set_false_path -from [get_clocks {*adc*}] -to [get_registers {*adc_sync*ff3*inst5*}]
set_false_path -from [get_clocks *] -to [get_ports {debug*}]
set_false_path -from [get_ports {LIVE L1A}] -to [get_ports {debug*}]
set_false_path -from [get_registers {*L2*fifo*}] -to [get_registers {*vme_interface*}]
set_false_path -from [get_ports {vme_iackin}] -to [get_ports {vme_iackout}]
set_false_path -from [get_registers *] -to [get_ports {Board*}]
set_false_path -from [get_ports {read_crc_error}] -to [get_registers {vme_interface*}]
set_false_path -from [get_keepers {new_packet_assembler:inst23|new_compression_block:inst5|new_coe_data_block:inst|coe_word:inst*}] -to [get_keepers {vme_data[*]}]


#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -setup -end -from [get_clocks {pll_125mhz}] -to [get_ports {*led*}] 3
set_multicycle_path -hold -end -from [get_clocks {pll_125mhz}] -to [get_ports {*led*}] 3
set_multicycle_path -setup -end -from [get_registers {*control*rom*inst157*}] -to [get_registers {*control*counter5*}] 2
set_multicycle_path -hold -end -from [get_registers {*control*rom*inst157*}] -to [get_registers {*control*counter5*}] 2
set_multicycle_path -setup -end -from [get_registers {*control*rom*inst157*}] -to [get_registers {*control*counter13*}] 2
set_multicycle_path -hold -end -from [get_registers {*control*rom*inst157*}] -to [get_registers {*control*counter13*}] 2
set_multicycle_path -setup -end -from [get_registers {*packet*compress*min_range*inst3|*}] -to [get_registers {*packet*compress*lossless*add7*}] 2
set_multicycle_path -hold -end -from [get_registers {*packet*compress*min_range*inst3|*}] -to [get_registers {*packet*compress*lossless*add7*}] 2
set_multicycle_path -setup -end -from [get_registers {*packet*compression*_cal*min_range*reg14en*}] -to [get_registers {*packet*compression*lossless*shifter_27*reg28*}] 2
set_multicycle_path -hold -end -from [get_registers {*packet*compression*_cal*min_range*reg14en*}] -to [get_registers {*packet*compression*lossless*shifter_27*reg28*}] 2
set_multicycle_path -setup -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {*adc_sync*aclr*}] 2
set_multicycle_path -hold -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {*adc_sync*aclr*}] 2
set_multicycle_path -setup -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {data_buffer*fifo*}] 2
set_multicycle_path -hold -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {data_buffer*fifo*}] 2
set_multicycle_path -setup -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {data_buffer*counter1*}] 2
set_multicycle_path -hold -end -from [get_registers {control:inst6|ff3:inst74|lpm_ff:lpm_ff_component|dffs[0]}] -to [get_registers {data_buffer*counter1*}] 2
set_multicycle_path -setup -end -from [get_registers {loopback*inst3*}] -to [get_registers {loopback*fifo*12a*}] 2
set_multicycle_path -hold -end -from [get_registers {loopback*inst3*}] -to [get_registers {loopback*fifo*12a*}] 2
set_multicycle_path -setup -end -from [get_registers {loopback*inst3*}] -to [get_registers {loopback*fifo*15a*}] 2
set_multicycle_path -hold -end -from [get_registers {loopback*inst3*}] -to [get_registers {loopback*fifo*15a*}] 2
set_multicycle_path -setup -end -from [get_registers {*control*sr4*[1]*}] -to [get_registers {*loopback_with*fifo*aclr*}] 2
set_multicycle_path -hold -end -from [get_registers {*control*sr4*[1]*}] -to [get_registers {*loopback_with*fifo*aclr*}] 2
set_multicycle_path -setup -end -from [get_registers {*pedestal_track*}] -to [get_ports {*vme_data[*}] 3
set_multicycle_path -hold -end -from [get_registers {*pedestal_track*}] -to [get_ports {*vme_data[*}] 3
set_multicycle_path -setup -end -from [get_registers {vme_interface*}] -to [get_registers {vme_interface*}] 2
set_multicycle_path -hold -end -from [get_registers {vme_interface*}] -to [get_registers {vme_interface*}] 2
set_multicycle_path -setup -end -from [get_registers {vme_interface*}] -to [get_ports {vme_* }] 3
set_multicycle_path -hold -end -from [get_registers {vme_interface*}] -to [get_ports {vme_* }] 3
set_multicycle_path -setup -end -from [get_registers {vme_interface*}] -to [get_ports {vme_data* vme_dtack}] 3
set_multicycle_path -hold -end -from [get_registers {vme_interface*}] -to [get_ports {vme_data* vme_dtack}] 3
set_multicycle_path -setup -end -from [get_registers {vme_interface*}] -to [get_ports {lckrefn_l1 lckrefn_l2}] 3
set_multicycle_path -hold -end -from [get_registers {vme_interface*}] -to [get_ports {lckrefn_l1 lckrefn_l2}] 3
set_multicycle_path -setup -end -from [get_ports {vme_ds* }] -to [get_ports {vme_data* vme_dir}] 3
set_multicycle_path -hold -end -from [get_ports {vme_ds* }] -to [get_ports {vme_data* vme_dir}] 3
set_multicycle_path -setup -end -from [get_ports {vme_ds* }] -to [get_registers {vme_interface*}] 2
set_multicycle_path -hold -end -from [get_ports {vme_ds* }] -to [get_registers {vme_interface*}] 2
set_multicycle_path -setup -end -from [get_ports {vme_as }] -to [get_ports {vme_data* vme_dir}] 3
set_multicycle_path -hold -end -from [get_ports {vme_as }] -to [get_ports {vme_data* vme_dir}] 3
set_multicycle_path -setup -end -from [get_ports {vme_as }] -to [get_registers {vme_interface*}] 2
set_multicycle_path -hold -end -from [get_ports {vme_as }] -to [get_registers {vme_interface*}] 2
set_multicycle_path -setup -end -from [get_ports {vme_data*}] -to [get_registers {vme_interface*reg*}] 2
set_multicycle_path -hold -end -from [get_ports {vme_data*}] -to [get_registers {vme_interface*reg*}] 2
set_multicycle_path -setup -end -from [get_registers {vme_interface*}] -to [get_registers *] 3
set_multicycle_path -hold -end -from [get_registers {vme_interface*}] -to [get_registers *] 3
set_multicycle_path -setup -end -from [get_registers *] -to [get_registers {vme*inst101*}] 3
set_multicycle_path -hold -end -from [get_registers *] -to [get_registers {vme*inst101*}] 3
set_multicycle_path -setup -end -from [get_registers {*vme_interface*vme_reg16*inst4*[12]*}] -to [get_ports {*chip_enable* }] 3
set_multicycle_path -hold -end -from [get_registers {*vme_interface*vme_reg16*inst4*[12]*}] -to [get_ports {*chip_enable* }] 3
set_multicycle_path -setup -end -from [get_keepers {loopback_with_daisy_chain:inst2|counter16:inst20|lpm_counter:lpm_counter_component|cntr_oqi:auto_generated|safe_q[10]}] -to [get_keepers {loopback_with_daisy_chain:inst2|counter16:inst20|lpm_counter:lpm_counter_component|cntr_oqi:auto_generated|safe_q[0]}] 2


#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

