module GetPeakSample(
	clk,
	wena,
	data,
	psample
);


input wire clk;
input wire wena;
input wire [13:0] data;

output reg [15:0] psample = 16'hffff;

reg [1:0] pipe_wena;
reg [13:0] hmax;
reg [15:0] counter;

always @( posedge clk)
begin
	
		pipe_wena = pipe_wena << 1;
		pipe_wena[0] = wena;
		
		if(pipe_wena[0]==1'b1 && pipe_wena[1]==1'b0)
		begin
			counter = 16'h0000;
			hmax = 5000;
			psample = 16'h0000;
		end
		
		counter = (wena==1'b1)? counter + 1'b1: counter;
		
		if(wena && data > hmax)
		begin
			hmax = data;
			psample = counter;
		end
		

end

endmodule
