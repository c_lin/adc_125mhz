module SLVDS_T(clk,rst,block,trigger,in,out,send,in_reg,dv,cb);

input wire clk;
input wire rst;
input wire [7:0] block;
input wire trigger;
input wire [15:0] in;


output reg out = 1'b0;
output reg dv = 1'b0;
output reg [15:0] cb = 16'b0;

reg [4:0] control = 5'b00000;
output reg send = 1'b0;
output reg [15:0] in_reg = 16'b0;
reg srst = 1'b0;

always @(posedge clk)
begin

	if(rst && ~send)
	begin
		send = 1'b0;
		in_reg = 16'b0;
		control = 5'b0;
		out = 1'b0;
		srst = 1'b0;
		dv = 1'b0;
		cb = 16'b0;
	end

	
	if(srst)
	begin
		send = 1'b0;
		control = 5'b00000;
		out = 1'b0;
		srst = 1'b0;
	end
	
	if(trigger==1'b1 && ~send )
	begin
		send = 1'b1;
		in_reg = in;
		control = 5'b0;
	end
	else 
	begin
		send = send;
	end
	
	
	if(send==1'b1)
	begin
		case(control)
			5'b00000:	
				begin
					out = 1'b1;
					dv = 1'b1;
					cb = in_reg;
				end	
			5'b00001:	
				begin
					out = 1'b1;
					dv = 1'b0;
					cb = 16'b0;
				end	
			5'b00010:	out = in_reg[0];
			5'b00011:	out = in_reg[1];
			5'b00100:	out = in_reg[2];
			5'b00101:	out = in_reg[3];
			5'b00110:	out = in_reg[4];
			5'b00111:	out = in_reg[5];
			5'b01000:	out = in_reg[6];
			5'b01001:	out = in_reg[7];
			5'b01010:	out = in_reg[8];
			5'b01011:	out = in_reg[9];
			5'b01100:	out = in_reg[10];
			5'b01101:	out = in_reg[11];
			5'b01110:	out = in_reg[12];
			5'b01111:	out = in_reg[13];
			5'b10000:	out = in_reg[14];
			5'b10001:				
				begin
						out = in_reg[15];	
						send = 1'b0;
						srst = 1'b1;
						in_reg = 16'b0;
				end
			default: 	out = 1'b0;	
		endcase	
	
		control = control + 1'b1;
	end
	else
	begin
		control = 0;
		out = 1'b0;
		dv = 1'b0;
		cb = 16'b0;
	end

end

endmodule
