module Tracking_Control(
	clk,
	start,
	done,
	
	mux_ena,
	cal_start,
	mux_switch,
	ena_switch
);

input wire clk;
input wire start;
input wire done;

output reg mux_ena = 1'b0;
output reg cal_start = 1'b0;
output reg [3:0] mux_switch = 4'b0;
output reg [15:0] ena_switch = 16'b0;

reg [1:0] pipeline_start;
reg ena = 1'b0;

reg [4:0] count_done = 5'b0; 
reg [2:0] control = 3'b0;

always @(posedge clk)
begin

	pipeline_start[1] <= pipeline_start[0];
	pipeline_start[0] <= start;
	
	if(pipeline_start[0]==1'b1 && pipeline_start[1]==1'b0)
	begin
		ena = 1'b1;
		mux_switch = 4'b0000;
		count_done = 5'b00000;
		control = 3'b0;
	end
	
	if(ena)
	begin
	
		mux_ena = 1'b1;
		
		if(control>=0 && control<=6)
		begin
			if(control == 3) 
			begin
				cal_start = 1'b0;
			end
			else if(control == 4)
			begin
				cal_start = 1'b1;		
			end		
			else if(control == 5)
			begin
				cal_start = 1'b0;		
			end
	
			control = control + 1'b1;
		end
		
		if(done==1'b1)
		begin 
			control = 3'b0;
			count_done = count_done + 1'b1;
			mux_switch = mux_switch + 1'b1;
			ena_switch = 1 << (count_done-1'b1);
			ena = (count_done==16)? 1'b0 : ena;
		end
		else
		begin
			ena_switch = 16'b0;
		end
		
	end
	else
	begin
		mux_ena = 1'b0;
		mux_switch = 4'b0000;
		ena_switch = 16'b0;
		control = 3'b0;
		count_done = 5'b0;
		cal_start = 1'b0;
	end
	

end


endmodule
