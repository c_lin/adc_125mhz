module VBit_Shifter (
	clk,
	shift0,
	shift1,
	shift_nhits,
	vb0,
	vb1,
	vb_out
);

input wire clk;
input wire [3:0] shift0;
input wire [3:0] shift1;
input wire [1:0] shift_nhits;
input wire [15:0] vb0;
input wire [15:0] vb1;
output reg [15:0] vb_out;

reg [1:0] shift_nhits_reg;
reg [4:0] sum;

always @(posedge clk)
begin

	shift_nhits_reg[1:0] = shift_nhits[1:0];

	vb_out = 16'b0;
	vb_out[shift0] = (vb0[15:0] != 16'b0)? 1'b1 : 1'b0;
	vb_out[shift1] = (vb1[15:0] != 16'b0)? 1'b1 : 1'b0;
	

	sum[4:0] = 	vb1[ 0] + vb1[ 1] + vb1[ 2] + vb1[ 3] + vb1[ 4] + vb1[ 5] + vb1[ 6] + vb1[ 7] +
					vb1[ 8] + vb1[ 9] + vb1[10] + vb1[11] + vb1[12] + vb1[13] + vb1[14] + vb1[15] ;
	

	sum[4:0] = (sum[4]==1'b1)? 5'b01111 : sum[4:0];
	
	case (shift_nhits_reg)
		2'b00: vb_out[15:8]  = 8'b0;
		2'b01: vb_out[11:8]  = sum[3:0];
		2'b10: vb_out[15:12] = sum[3:0];
	endcase			


end

endmodule
