module Gen_EventCnt (

	input clk,
	input rst,
	input live,
	input get_packet,

	output reg [15:0] ievt

);


always @(posedge clk)
begin
	
	if(rst | ~live ) 
	begin
		ievt = 16'b0;
	end
	
	if( get_packet==1'b1 )
	begin
		ievt = ievt + 1'b1;
	end
	 
end

endmodule
