module Data_Gate(
	clk,
	live,
	trig,
	
	sw
);

input wire clk;
input wire live;
input wire trig;

output reg sw = 1'b0;

reg [9:0] pipe_trig = 10'b0;

always @(posedge clk)
begin
	

	pipe_trig = pipe_trig << 1;
	pipe_trig[0] = trig;
	
	if(~live)
	begin
		sw = 1'b0;
	end
	
	sw = (~sw && pipe_trig[9]==1'b1)? 1'b1 : sw;
	
	
	
end

endmodule
