module Veto_CalSum (
	clk,
	in0,
	in1,
	lock,
	out
);

input wire clk;
input wire lock;
input wire [15:0] in0;
input wire [15:0] in1;

output reg [15:0] out;


reg [15:0] sum;
reg [9:0] pipeline_lock;

reg [4:0] sum0;
reg [4:0] sum1;

always @(posedge clk)
begin
		
	pipeline_lock <= pipeline_lock << 1;
	pipeline_lock[0] = lock;
	
	
	
	if(pipeline_lock[9]==1'b0)
	begin
		out = in0 + in1;
	end
	else
	begin
		sum[7:0] = in0[7:0] + in1[7:0];
	
		sum0[4:0] = in0[11:8] + in1[11:8];
		sum0[4:0] = (sum0[4]==1'b1)? 5'b01111 : sum0[4:0];
		
		sum1[4:0] = in0[15:12] + in1[15:12];
		sum1[4:0] = (sum1[4]==1'b1)? 5'b01111 : sum1[4:0];
		
		out[15:0] = {sum1[3:0], sum0[3:0], sum[7:0]};
	end	
	
end

endmodule
