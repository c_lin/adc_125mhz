module Send_TLKError(
	clk,
	err,
	send,
	out,
	switch
);

input wire clk;
input wire err;
input wire send;
output reg out = 1'b0;
output reg switch = 1'b0;

reg ena = 1'b0;
reg [1:0] control = 2'b0;



always @( posedge clk)
begin
	ena = (send==1'b1)? 1'b1 : ena;
	
	if(ena)
	begin
	
		if(control==0)
		begin
			out = 1'b1;
			switch = 1'b1;
		end
		else if(control==1)
		begin
			out = 1'b0;
			switch = 1'b1;
		end
		else if(control==2)
		begin
			out = err;
			ena = 1'b0;
			switch = 1'b1;
		end
	
		control = control + 1'b1;
	end
	else 
	begin
	
		out = 1'b0;
		control = 2'b0;
		switch = 1'b0;
		
	end
end

endmodule
