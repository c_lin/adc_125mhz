module MEM_Reader (

	input clk,
	input rst,
	input load_packet,
	input [11:0] waddr,
	
	output reg get_packet,
	output reg rena,
	output reg [11:0] raddr_1stSample,
	output reg [11:0] raddr,
	output reg [1:0] header
);

reg [7:0] local_waddr = 0;
reg [7:0] local_raddr = 0;
reg [11:0] nevnt = 0;
reg [10:0] cnt_control = 0;
reg [10:0] cnt_words = 0;
reg [11:0] raddr_reg = 0;
reg [3:0] gap = 4'b0110;
reg [3:0] load_packet_pipe = 0;
reg [1:0] header_delay0 = 0;
reg [1:0] header_delay1 = 0;
reg [1:0] header_delay2 = 0;

reg rden = 1'b0;
reg wren = 1'b0;
wire [11:0] q;

/*
module MEM_ADDR (
	clock,
	data,
	rdaddress,
	rden,
	wraddress,
	wren,
	q);
*/
MEM_ADDR _ram(clk,waddr,local_raddr,rden,local_waddr,wren,q);

always @ (posedge clk)
begin

	if(rst)
	begin
		rden = 1'b0;
		wren = 1'b0;
		local_waddr = 8'b0;
		local_raddr = 8'b0;
		nevnt = 0;
		cnt_control = 0;
		cnt_words = 0;
		raddr_reg = 0;
		load_packet_pipe = 0;
		header_delay0 = 0;
		header_delay1 = 0;
		header_delay2 = 0;
	end

	
	load_packet_pipe = load_packet_pipe << 1;
	load_packet_pipe[0] = load_packet;

	if(load_packet_pipe[1])
	begin
		wren = 1'b1;
		//ram[local_waddr] = waddr;
		//local_waddr = local_waddr + 1'b1;
		//nevnt = nevnt + 1'b1;
	end
	else
	begin
		wren = 1'b0;
	end	

	if(load_packet_pipe[2])
	begin
		local_waddr = local_waddr + 1'b1;
		nevnt = nevnt + 1'b1;
	end	
	
	if(nevnt!=0)
	begin	
		if( cnt_control < gap ) // headers
		begin
			rden = 1'b1;
			raddr_reg = q;//ram[local_raddr];
			get_packet = (cnt_control==0)? 1'b1 : 1'b0;
			header_delay0 = 2'b11;
			rena = 1'b0;
			raddr_1stSample = raddr_reg;
			raddr = raddr_reg;
			cnt_words = 0;
			cnt_control = cnt_control + 1'b1;
		end		
		else if( cnt_control >= gap )
		begin
			rden = 1'b0;
			get_packet = 1'b0;
		
			if( cnt_words < 1024 )
			begin
				header_delay0 = 2'b10;
				rena = 1'b1;
				raddr = raddr_reg + cnt_words[10:4];	
				cnt_words = cnt_words + 1'b1;			
				cnt_control = cnt_control + 1'b1;	
			end
			else if( cnt_words == 1024 ) // after the last energy word
			begin
				header_delay0 = 2'b01;
				rena = 1'b0;
				cnt_words = cnt_words + 1'b1;		
				cnt_control = cnt_control + 1'b1;	
				//cnt_control = 0;
				//cnt_words = 0;
				//local_raddr = local_raddr + 1'b1;
				//nevnt = nevnt - 1'b1;
			end
			else //if( cnt_words > 1024 ) // after footer
			begin
				header_delay0 = 2'b00;
				rena = 1'b0;
				rden = 1'b0;
				cnt_control = 0;
				cnt_words = 0;
				local_raddr = local_raddr + 1'b1;
				nevnt = nevnt - 1'b1;
			end						
		end		
	end	
	else // no event in MEM
	begin
		get_packet = 1'b0;
		header_delay0 = 2'b00;
		rena = 1'b0;
		cnt_words = 0;
		cnt_control = 0;
		raddr = waddr - 1'b1;
		rden = 1'b0;
	end	
	
	header = header_delay2;	
	header_delay2 = header_delay1;
	header_delay1 = header_delay0;
	
	
end

endmodule

