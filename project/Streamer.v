module Streamer(
clk,
rst,
DCID,
ADCID,
timestamp,
counter00,
counter01,
counter02,
counter03,
counter04,
counter05,
counter06,
counter07,
counter08,
counter09,
counter10,
counter11,
counter12,
counter13,
counter14,
counter15,
out
);


input wire clk;
input wire rst;
input wire [1:0]  DCID;
input wire [15:0] ADCID;
input wire [28:0] timestamp;
input wire [15:0] counter00;
input wire [15:0] counter01;
input wire [15:0] counter02;
input wire [15:0] counter03;
input wire [15:0] counter04;
input wire [15:0] counter05;
input wire [15:0] counter06;
input wire [15:0] counter07;
input wire [15:0] counter08;
input wire [15:0] counter09;
input wire [15:0] counter10;
input wire [15:0] counter11;
input wire [15:0] counter12;
input wire [15:0] counter13;
input wire [15:0] counter14;
input wire [15:0] counter15;


output reg out;

reg srst = 1'b0;
reg [15:0] data;
reg [15:0] counter;
reg [8:0] control;
reg [4:0] switch;
reg [3:0] ibit;
reg send;


always @(posedge clk)
begin

	if(rst || srst)
	begin
		srst = 1'b0;
		send = 1'b0;
		switch = 5'b0;
		ibit = 4'b0;
		out = 1'b0;
	end
	
	if(ibit==4'b0000)
	begin
		case(switch)
			5'b00000:	data[15:0] = 16'h0000;
			5'b00001:	data[15:0] = 16'h0000;
			5'b00010:	data[15:0] = 16'hFEFE;
			5'b00011:	data[15:0] = ADCID;
			5'b00100:	data[15:0] = counter00[15:0];
			5'b00101:	data[15:0] = counter01[15:0];
			5'b00110:	data[15:0] = counter02[15:0];
			5'b00111:	data[15:0] = counter03[15:0];
			5'b01000:	data[15:0] = counter04[15:0];
			5'b01001:	data[15:0] = counter05[15:0];
			5'b01010:	data[15:0] = counter06[15:0];
			5'b01011:	data[15:0] = counter07[15:0];
			5'b01100:	data[15:0] = counter08[15:0];
			5'b01101:	data[15:0] = counter09[15:0];
			5'b01110:	data[15:0] = counter10[15:0];
			5'b01111:	data[15:0] = counter11[15:0];
			5'b10000:	data[15:0] = counter12[15:0];
			5'b10001:	data[15:0] = counter13[15:0];
			5'b10010:	data[15:0] = counter14[15:0];
			5'b10011:	data[15:0] = counter15[15:0];
			5'b10100:	data[15:0] = 16'h0000;
		endcase	
	end	
	

	if( timestamp[1:0] == DCID[1:0] )
	begin
	
		out = data[ibit];
		
		if( ibit[3:0] != 4'b1111 )
		begin
			ibit = ibit + 1'b1;
		end	
		else 
		begin
			srst = (switch==5'b10101)? 1'b1 : 1'b0;
			switch = switch + 1'b1;
			ibit = 4'b0000; 
		end	
		
	end
	else 
	begin
	
		out = 1'b0;
		
	end

end

endmodule
