module Derivative3 (
	clk,
	rst,
	thres,
	in0,
	dv0,
	in1,
	dv1,
	cnt_out
);

input wire clk;
input wire rst;
input wire [15:0] thres;
input wire [15:0] in0;
input wire dv0;
input wire [15:0] in1;
input wire dv1;

output reg [15:0] cnt_out; 

reg [15:0] diff; 
reg [14:0] counter;

always @(posedge clk)
begin

	if(rst)
	begin	
		counter = 15'b0;
	end

	
	if( dv0 && dv1 )
	begin
		diff = (in0 > in1)? in0 - in1 : in1 - in0;			
	end
	else 
	begin
		diff = 15'b0;
	end
	
	if( diff >= thres )
	begin
		counter = (counter == 16'h7FFF)? 16'h7FFF : counter + 1'b1;
	end
	
	cnt_out = {1'b0, counter};
	
end

endmodule
