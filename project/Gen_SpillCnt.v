module Gen_SpillCnt (

	input clk,
	input rst,
	input live,

	output reg [9:0] ispill

);

reg [1:0] live_pipe = 0; 


always @(posedge clk)
begin

	live_pipe = live_pipe << 1;
	live_pipe[0] = live;
	
	if(rst) 
	begin
		ispill = 0;
	end
	
	if( live_pipe[1]==1'b0 && live_pipe[0]==1'b1 )
	begin
		ispill = ispill + 1'b1;
	end
	 
end

endmodule
