module Count_CBDelay (
	clk,
//	LIVE,
	dv_CB,
	load_N_samples,
	
	delay
);


input wire clk;
//input wire LIVE;
input wire dv_CB;
input wire load_N_samples;

output reg [15:0] delay = 16'b0;

reg ena = 1'b0;

reg [1:0] pipe_dv;
reg [1:0] pipe_load;
//reg [1:0] pipe_live;

always @(posedge clk)
begin

	pipe_dv <= pipe_dv << 1;
	pipe_dv[0] <= dv_CB;
	
	pipe_load <= pipe_load << 1;
	pipe_load[0] <= load_N_samples;
	
//	pipe_live <= pipe_live << 1;
//	pipe_live[0] <= LIVE;
/*
	if(pipe_live[0]==1'b1 && pipe_live[1]==1'b0) 
	begin
		ena = 1'b0;
		delay = 16'b0 - 16'b100;
	end	
*/	
	//ena = (pipe_dv[0]==1'b1 && pipe_dv[1]==1'b0)? 1'b1 : ena;
	
	if(pipe_dv[0]==1'b1 && pipe_dv[1]==1'b0)
	begin
		ena = 1'b1;
		delay = 16'b0 - 16'b100;
	end
	
	ena = (pipe_load[0]==1'b1 && pipe_load[1]==1'b0)? 1'b0 : ena;

	if( ena ) delay = delay + 1'b1;
	
end

endmodule
