module Gen_TimeStamp (

	input clk,
	input rst,
	input live,
	input load_packet,
	input get_packet,

	output reg [28:0] timestamp

);

//reg [28:0] ram [255:0];
reg [28:0] cnt_time = 29'b0;
reg [7:0] local_waddr = 8'b0;
reg [7:0] local_raddr = 8'b0;


reg [3:0] load_packet_pipe = 4'b0;
reg [3:0] get_packet_pipe = 4'b0;

reg rden = 1'b0;
reg wren = 1'b0;
wire [28:0] q;

/*
module MEM_ADDR (
	clock,
	data,
	rdaddress,
	rden,
	wraddress,
	wren,
	q);
*/
MEM_TS _ram(clk,cnt_time,local_raddr,rden,local_waddr,wren,q);


always @(posedge clk)
begin

	load_packet_pipe = load_packet_pipe << 1;
	load_packet_pipe[0] = load_packet;
	
	get_packet_pipe = get_packet_pipe << 1;
	get_packet_pipe[0] = get_packet;
	
	//if(rst | ~live)
	if(~live)
	begin
		cnt_time = 29'b0;
		local_waddr = 8'b0;
		local_raddr = 8'b0;
		load_packet_pipe = 4'b0;
		get_packet_pipe = 4'b0;
		timestamp = 29'b0;
	end
	else
	begin
		cnt_time = cnt_time + 1'b1;
	end

		
	wren = (load_packet_pipe[0])?	1'b1 : 1'b0;		
	local_waddr = (load_packet_pipe[3])? local_waddr + 1'b1 : local_waddr;
	
	rden = (get_packet_pipe[0])? 1'b1 : 1'b0;
	local_raddr = (get_packet_pipe[3])? local_raddr + 1'b1 : local_raddr;

	timestamp = q;
	
	
	
end

endmodule
