module Add_Offset(
    input [29:0] dataa,
    input [31:0] datab,
    input clk,
	 input lt,
    output reg [29:0] result
);



    always @ (posedge clk)
    begin
        if(datab[31]==1'b0)
            result[29:0] = dataa[29:0] + datab[29:0];
        else
            result[29:0] = dataa[29:0] - datab[29:0];
				
		  if(lt)
				result[29:0] = 30'b0;
    end

endmodule
