module CB_Delay(
	clk,
	delay,
	CB_in,
	CB_out
);

input wire clk;
input wire [8:0] delay;
input wire [15:0] CB_in;

output reg [15:0] CB_out;

reg [8:0] raddr = 1'b0;
reg [8:0] waddr = 1'b1;
wire [15:0] q_wire;

Et_Buffer cb_buffer(clk,CB_in,raddr,1'b1,waddr,1'b1,q_wire);

always @(posedge clk)
begin

	waddr = waddr + 1'b1;
	raddr = waddr -1'b1 - delay;
	CB_out = q_wire;

end

endmodule

