module CL_align(clk,load,cnt);

input wire clk;
input wire load;

output reg [7:0] cnt = 8'b0;

reg load_reg;

always @(posedge clk)
begin
	
	cnt = (load_reg&&~load)? 8'b0:cnt;
	cnt = (load)? cnt+1'b1:cnt;
	
	load_reg = load;
end
endmodule