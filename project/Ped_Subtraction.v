module Ped_Subtraction(
	clk,
	data_in,
	ped,
	data_out
);

input wire clk;
input wire [13:0] data_in;
input wire [13:0] ped;

output reg [13:0] data_out;

always @(posedge clk)
begin
	if( ped <= data_in )
	begin
		data_out <= data_in - ped;
	end
	else
	begin
		data_out <= 14'b0;
	end
end

endmodule
