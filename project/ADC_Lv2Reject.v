module ADC_Lv2Reject(

	input clk,
	//input live,
	input wena,
	input [7:0] margin,
	input [11:0] waddr,
	input [11:0] raddr,
	output reg busy
);

reg [11:0] waddr_reg = 12'b1;
reg [11:0] raddr_reg = 12'b0;
reg [11:0] margin_reg = 12'b0;

always @(posedge clk)
begin

	busy = 1'b0;

	if( (waddr_reg > raddr_reg) && (12'hFFF - waddr_reg + raddr_reg) < margin_reg ) busy = 1'b1;
	if( (waddr_reg < raddr_reg) && (raddr_reg - waddr_reg) < margin_reg ) busy = 1'b1;

	waddr_reg = (wena)? waddr : waddr_reg;
	raddr_reg = raddr;
	margin_reg = { 4'b0000, margin};
	
end
endmodule