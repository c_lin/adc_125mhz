module Register_Bank (
	addr,
	value,
	reg01,
	reg02,
	reg03,
	reg04,
	reg05,
	reg06,
	reg07,
	reg08,
	reg09,
	reg10,
	reg11,
	reg12,
	reg13,
	reg14,
	reg15,
	reg16,
	reg17,
	reg18,
	reg19,
	reg20,
	reg21,
	reg22,
	reg23,
	reg24,
	reg25,
	reg26,
	reg27,
	reg28,
	reg29,
	reg30,
	reg31,
	reg32,
	
	reg33,
	reg34,
	reg35,
	reg36,
	reg37,
	reg38,
	reg39,
	reg40,
	reg41,
	reg42,
	reg43,
	reg44,
	reg45,
	
	reg46,
	reg47,
	reg48,
	reg49,
	reg50,
	reg51,
	reg52,
	reg53,
	reg54,
	reg55,
	reg56,
	reg57,
	reg58,
	reg59,
	reg60,	
	
	clk
);

input wire clk;
input wire [5:0] addr;
input wire [31:0] value;

output reg [31:0] reg01;
output reg [31:0] reg02;
output reg [31:0] reg03;
output reg [31:0] reg04;
output reg [31:0] reg05;
output reg [31:0] reg06;
output reg [31:0] reg07;
output reg [31:0] reg08;
output reg [31:0] reg09;
output reg [31:0] reg10;
output reg [31:0] reg11;
output reg [31:0] reg12;
output reg [31:0] reg13;
output reg [31:0] reg14;
output reg [31:0] reg15;
output reg [31:0] reg16;
output reg [31:0] reg17;
output reg [31:0] reg18;
output reg [31:0] reg19;
output reg [31:0] reg20;
output reg [31:0] reg21;
output reg [31:0] reg22;
output reg [31:0] reg23;
output reg [31:0] reg24;
output reg [31:0] reg25;
output reg [31:0] reg26;
output reg [31:0] reg27;
output reg [31:0] reg28;
output reg [31:0] reg29;
output reg [31:0] reg30;
output reg [31:0] reg31;
output reg [31:0] reg32;

output reg [31:0] reg33;
output reg [31:0] reg34;
output reg [31:0] reg35;
output reg [31:0] reg36;
output reg [31:0] reg37;
output reg [31:0] reg38;
output reg [31:0] reg39;
output reg [31:0] reg40;
output reg [31:0] reg41;
output reg [31:0] reg42;
output reg [31:0] reg43;
output reg [31:0] reg44;
output reg [31:0] reg45;

output reg [31:0] reg46;
output reg [31:0] reg47;
output reg [31:0] reg48;
output reg [31:0] reg49;
output reg [31:0] reg50;
output reg [31:0] reg51;
output reg [31:0] reg52;
output reg [31:0] reg53;
output reg [31:0] reg54;
output reg [31:0] reg55;
output reg [31:0] reg56;
output reg [31:0] reg57;
output reg [31:0] reg58;
output reg [31:0] reg59;
output reg [31:0] reg60;

reg [5:0] switch = 6'b0; 

always @(posedge clk)
begin

	switch = addr;
	if(switch>0)
	begin
				case(switch)
					6'b000001:	reg01 = value;
					6'b000010:	reg02 = value;
					6'b000011:	reg03 = value;
					6'b000100:	reg04 = value;
					6'b000101:	reg05 = value;
					6'b000110:	reg06 = value;
					6'b000111:	reg07 = value;
					6'b001000:	reg08 = value;
					6'b001001:	reg09 = value;
					6'b001010:	reg10 = value;
					6'b001011:	reg11 = value;
					6'b001100:	reg12 = value;
					6'b001101:	reg13 = value;
					6'b001110:	reg14 = value;
					6'b001111:	reg15 = value;
					6'b010000:	reg16 = value;
					6'b010001:	reg17 = value;
					6'b010010:	reg18 = value;
					6'b010011:	reg19 = value;
					6'b010100:	reg20 = value;
					6'b010101:	reg21 = value;
					6'b010110:	reg22 = value;
					6'b010111:	reg23 = value;
					6'b011000:	reg24 = value;
					6'b011001:	reg25 = value;
					6'b011010:	reg26 = value;
					6'b011011:	reg27 = value;
					6'b011100:	reg28 = value;
					6'b011101:	reg29 = value;
					6'b011110:	reg30 = value;
					6'b011111:	reg31 = value;
					6'b100000:	reg32 = value;
					
					6'b100001:	reg33 = value;
					6'b100010:	reg34 = value;
					6'b100011:	reg35 = value;
					6'b100100:	reg36 = value;
					6'b100101:	reg37 = value;
					6'b100110:	reg38 = value;
					6'b100111:	reg39 = value;
					6'b101000:	reg40 = value;
					6'b101001:	reg41 = value;
					6'b101010:	reg42 = value;
					6'b101011:	reg43 = value;
					6'b101100:	reg44 = value;
					6'b101101:	reg45 = value;
					
					6'b101110:	reg46 = value;
					6'b101111:	reg47 = value;				
					6'b110000:	reg48 = value;
					6'b110001:	reg49 = value;
					6'b110010:	reg50 = value;
					6'b110011:	reg51 = value;
					6'b110100:	reg52 = value;
					6'b110101:	reg53 = value;
					6'b110110:	reg54 = value;
					6'b110111:	reg55 = value;
					6'b111000:	reg56 = value;
					6'b111001:	reg57 = value;
					6'b111010:	reg58 = value;
					6'b111011:	reg59 = value;
					6'b111100:	reg60 = value;					
				endcase
		end	

end
endmodule
