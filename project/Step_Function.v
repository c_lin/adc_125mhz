module Step_Function(
	clk,
	rst,
	out14,
	out16
);

input wire clk;
input wire rst;
output reg [13:0] out14;
output reg [15:0] out16;

reg [3:0] control;

always @(posedge clk)
begin
	
	if(rst)
	begin
		control = 4'b0;
	end
	
	if(control<8)
	begin
		out14 = 14'b0;
		out16 = 16'b0;
	end
	else
	begin
		out14 = 14'b1;
		out16 = 16'b1;
	end
	
	control = control + 1'b1;
	
end

endmodule 