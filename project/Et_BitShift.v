module Et_BitShift (
	clk,
	sum_in,
	sum_out
);

input wire clk;
input wire [33:0] sum_in;

output reg [15:0] sum_out;


always @( posedge clk)
begin
	
	// 
	sum_out[15] = (sum_in[33:27]!=7'b0000000)? 1'b1 : 1'b0;
	
	if(sum_out[15]==1'b0)
		sum_out[14:0] = sum_in[26:12];
	else 
		sum_out[14:0] = sum_in[28:14];	
	
end

endmodule
