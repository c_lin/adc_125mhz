module GenPulse(
	clk,
	mode,
	trig,
	data14,
	switch
);


input wire clk;
input wire [2:0] mode;
input wire trig;

output reg [13:0] data14;

output reg switch = 1'b0;

reg [1:0] pipeline;

reg [1:0] control = 2'b00;
reg ena = 1'b0;
reg trig_cnt = 1'b0; 
reg [2:0] mode_reg;

always @(posedge clk)
begin
	
	mode_reg = mode;
	
	pipeline = pipeline << 1;
	pipeline[0] = trig;
	
	if( pipeline[0]==1'b1 && pipeline[1]==1'b0 )
	begin
		ena = 1'b1;
		control = 2'b00;
		trig_cnt = trig_cnt + 1'b1;
	end
	
	if(ena)
	begin	
	
		switch = 1'b1;
		
		case(control)
			2'b00: data14[13:0] = 14'b0;
			2'b01:
				begin
					case(mode_reg)
						3'b000: 	data14[13:0] = 10000;
						3'b001: 	data14[13:0] = 100;
						3'b010:  data14[13:0] = 1;
						3'b011:  data14[13:0] = 14'b0;
						3'b100:  data14[13:0] = (trig_cnt==1'b1)? 100 : 10000;
						default: data14[13:0] = 10000;
					endcase
				end	
			2'b10:
				begin
					data14[13:0] = 14'b0;
					ena = 1'b0;			
				end
			default: data14[13:0] = 14'b0;	
		endcase
		
		control = control + 1'b1;
		
	end
	else
	begin
		switch = 1'b0;
		control = 2'b00;
	end
	
end

endmodule
