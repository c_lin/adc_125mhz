module GenDelta(
	clk,
	pattern_tx,	
	trig,
	out,
	switch
);


input wire clk;
input wire [15:0] pattern_tx;
input wire trig;

output reg [15:0] out = 999;
output reg switch = 1'b0;

reg [63:0] pipeline;

reg [1:0] control = 2'b00;
reg ena = 1'b0;

always @(posedge clk)
begin
	
	pipeline <= pipeline << 1;
	pipeline[0] <= trig;
	
	if( pipeline[0]==1'b1 && pipeline[1]==1'b0 )
	begin
		ena = 1'b1;
		control = 2'b00;
	end
	
	if(ena)
	begin	
		switch = 1'b1;
		if(control==0)
		begin
			out = 16'b0;
		end
		else if(control==1)
		begin
			out = pattern_tx;
		end
		else if(control==2)
		begin
			out = 16'b0;
			ena = 1'b0;			
		end

		control = control + 1'b1;
	end
	else
	begin
		switch = 1'b0;
		control = 2'b00;
		out = 16'b0;
	end
	
end

endmodule
