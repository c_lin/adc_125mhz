module CBDelay_Scanner(
	clk,
	LIVE,
	ena,
	start,
	stop,
	cb,
	
	delay,
	sw,
	delay00,
	delay01,
	delay02,
	delay03,
	delay04,
	delay05,
	delay06,
	delay07,
	delay08,
	delay09,
	delay10,
	delay11,
	delay12,
	delay13,
	delay14,
	delay15,
	
	nhits00,
	nhits01,
	nhits02,
	nhits03,
	nhits04,
	nhits05,
	nhits06,
	nhits07,
	nhits08,
	nhits09,
	nhits10,
	nhits11,
	nhits12,
	nhits13,
	nhits14,
	nhits15
	
);

input wire clk;
input wire LIVE;
input wire ena;
input wire [8:0] start;
input wire [8:0] stop;
input wire [15:0] cb;


output reg [15:0] delay00;
output reg [15:0] delay01;
output reg [15:0] delay02;
output reg [15:0] delay03;
output reg [15:0] delay04;
output reg [15:0] delay05;
output reg [15:0] delay06;
output reg [15:0] delay07;
output reg [15:0] delay08;
output reg [15:0] delay09;
output reg [15:0] delay10;
output reg [15:0] delay11;
output reg [15:0] delay12;
output reg [15:0] delay13;
output reg [15:0] delay14;
output reg [15:0] delay15; 

output reg [15:0] nhits00;
output reg [15:0] nhits01;
output reg [15:0] nhits02;
output reg [15:0] nhits03;
output reg [15:0] nhits04;
output reg [15:0] nhits05;
output reg [15:0] nhits06;
output reg [15:0] nhits07;
output reg [15:0] nhits08;
output reg [15:0] nhits09;
output reg [15:0] nhits10;
output reg [15:0] nhits11;
output reg [15:0] nhits12;
output reg [15:0] nhits13;
output reg [15:0] nhits14;
output reg [15:0] nhits15;

output reg [8:0] delay; 
output reg sw = 1'b0;


reg scanning = 1'b0;
reg [1:0] pipe_ena;
reg [1:0] pipe_live;





reg [8:0] delay_keep [15:0];

reg [15:0] max_hits [15:0];

reg [15:0] counts [15:0];

integer i;

always @(posedge clk)
begin
	
	pipe_ena = pipe_ena << 1;
	pipe_ena[0] = ena;
	
	pipe_live = pipe_live << 1;
	pipe_live[0] = LIVE;
	
	if(pipe_ena[0]==1'b1 && pipe_ena[1]==1'b0)
	begin
		for(i=0;i<16;i=i+1)
		begin
			max_hits[i] = 16'b0;
			delay_keep[i] = 16'b0;
			counts[i] = 16'b0;
		end
	
		scanning = 1'b1;
		delay = start;
	end
	
	if(scanning && ena)
	begin
	
		sw = 1'b1;

		counts[ 0] = (cb[ 0]==1'b1 && counts[ 0]<16'hFFFF && LIVE)? counts[ 0] + 1'b1 : counts[ 0];
		counts[ 1] = (cb[ 1]==1'b1 && counts[ 1]<16'hFFFF && LIVE)? counts[ 1] + 1'b1 : counts[ 1];
		counts[ 2] = (cb[ 2]==1'b1 && counts[ 2]<16'hFFFF && LIVE)? counts[ 2] + 1'b1 : counts[ 2];
		counts[ 3] = (cb[ 3]==1'b1 && counts[ 3]<16'hFFFF && LIVE)? counts[ 3] + 1'b1 : counts[ 3];
		counts[ 4] = (cb[ 4]==1'b1 && counts[ 4]<16'hFFFF && LIVE)? counts[ 4] + 1'b1 : counts[ 4];
		counts[ 5] = (cb[ 5]==1'b1 && counts[ 5]<16'hFFFF && LIVE)? counts[ 5] + 1'b1 : counts[ 5];
		counts[ 6] = (cb[ 6]==1'b1 && counts[ 6]<16'hFFFF && LIVE)? counts[ 6] + 1'b1 : counts[ 6];
		counts[ 7] = (cb[ 7]==1'b1 && counts[ 7]<16'hFFFF && LIVE)? counts[ 7] + 1'b1 : counts[ 7];
		counts[ 8] = (cb[ 8]==1'b1 && counts[ 8]<16'hFFFF && LIVE)? counts[ 8] + 1'b1 : counts[ 8];
		counts[ 9] = (cb[ 9]==1'b1 && counts[ 9]<16'hFFFF && LIVE)? counts[ 9] + 1'b1 : counts[ 9];
		counts[10] = (cb[10]==1'b1 && counts[10]<16'hFFFF && LIVE)? counts[10] + 1'b1 : counts[10];
		counts[11] = (cb[11]==1'b1 && counts[11]<16'hFFFF && LIVE)? counts[11] + 1'b1 : counts[11];
		counts[12] = (cb[12]==1'b1 && counts[12]<16'hFFFF && LIVE)? counts[12] + 1'b1 : counts[12];
		counts[13] = (cb[13]==1'b1 && counts[13]<16'hFFFF && LIVE)? counts[13] + 1'b1 : counts[13];
		counts[14] = (cb[14]==1'b1 && counts[14]<16'hFFFF && LIVE)? counts[14] + 1'b1 : counts[14];
		counts[15] = (cb[15]==1'b1 && counts[15]<16'hFFFF && LIVE)? counts[15] + 1'b1 : counts[15];
	
		if(pipe_live[0]==1'b1 && pipe_live[1]==1'b0) // LIVE rising edge
		begin
			for(i=0;i<16;i=i+1)
			begin
				counts[i] = 16'b0;
			end
		end
	
		if(pipe_live[0]==1'b0 && pipe_live[1]==1'b1) // LIVE falling edge
		begin	
			for(i=0;i<16;i=i+1)
			begin
				if(counts[i]>max_hits[i])
				begin
					max_hits[i] = counts[i];
					delay_keep[i] = delay;
				end
			end	
			delay = delay + 1'b1;
		end
		
		scanning = (delay==stop)? 1'b0 : scanning;
		
	end // end of scanning	
	else 
	begin
	
		sw = 1'b0;
		
	end
	
	delay00 = { 7'b0000000, delay_keep[ 0] };
	delay01 = { 7'b0000000, delay_keep[ 1] };
	delay02 = { 7'b0000000, delay_keep[ 2] };
	delay03 = { 7'b0000000, delay_keep[ 3] };
	delay04 = { 7'b0000000, delay_keep[ 4] };
	delay05 = { 7'b0000000, delay_keep[ 5] };
	delay06 = { 7'b0000000, delay_keep[ 6] };
	delay07 = { 7'b0000000, delay_keep[ 7] };
	delay08 = { 7'b0000000, delay_keep[ 8] };
	delay09 = { 7'b0000000, delay_keep[ 9] };
	delay10 = { 7'b0000000, delay_keep[10] };
	delay11 = { 7'b0000000, delay_keep[11] };
	delay12 = { 7'b0000000, delay_keep[12] };
	delay13 = { 7'b0000000, delay_keep[13] };
	delay14 = { 7'b0000000, delay_keep[14] };
	delay15 = { 7'b0000000, delay_keep[15] };

	nhits00 = max_hits[ 0];
	nhits01 = max_hits[ 1];
	nhits02 = max_hits[ 2];
	nhits03 = max_hits[ 3];
	nhits04 = max_hits[ 4];
	nhits05 = max_hits[ 5];
	nhits06 = max_hits[ 6];
	nhits07 = max_hits[ 7];
	nhits08 = max_hits[ 8];
	nhits09 = max_hits[ 9];
	nhits10 = max_hits[10];
	nhits11 = max_hits[11];
	nhits12 = max_hits[12];
	nhits13 = max_hits[13];
	nhits14 = max_hits[14];
	nhits15 = max_hits[15];
	
end

endmodule 
