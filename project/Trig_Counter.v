module Trig_Counter (
	clk,
	rst,
	L1A,
	PL1A,
	PS,
	ALIGN,
	DELTA,
	sw,
	out
);


input wire clk;
input wire rst;
input wire L1A;
input wire PL1A;
input wire PS;
input wire ALIGN;
input wire DELTA;
input wire [2:0] sw;


output reg [15:0] out = 16'b0;

reg [2:0] sw_reg;
reg [15:0] cnt_L1A = 16'b0;
reg [15:0] cnt_PL1A = 16'b0;
reg [15:0] cnt_PS = 16'b0;
reg [15:0] cnt_ALIGN = 16'b0;
reg [15:0] cnt_DELTA = 16'b0;

always @(posedge clk)
begin

	if(rst)
	begin	
		cnt_L1A = 16'b0;
		cnt_PL1A = 16'b0;
		cnt_PS = 16'b0;
		cnt_ALIGN = 16'b0;
		cnt_DELTA = 16'b0;
	end
	
	cnt_L1A 		= (L1A==1'b1)? 	cnt_L1A + 1'b1 	: cnt_L1A;
	cnt_PL1A 	= (PL1A==1'b1)? 	cnt_PL1A + 1'b1 	: cnt_PL1A;
	cnt_PS 		= (PS==1'b1)? 		cnt_PS + 1'b1 		: cnt_PS;
	cnt_ALIGN 	= (ALIGN==1'b1)? 	cnt_ALIGN + 1'b1 	: cnt_ALIGN;
	cnt_DELTA 	= (DELTA==1'b1)? 	cnt_DELTA + 1'b1 	: cnt_DELTA;
	
	sw_reg = sw;

	case(sw_reg)
		3'b000: out = cnt_L1A;
		3'b001: out = cnt_PL1A;
		3'b010: out = cnt_PS;
		3'b011: out = cnt_ALIGN;
		3'b100: out = cnt_DELTA;
	endcase
	
end

endmodule
