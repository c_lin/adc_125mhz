module SLVDS_test(clk,trigger,adc_id,switch,out);

input wire clk;
input wire [7:0] adc_id;
input wire switch;

output reg trigger;
output reg [15:0] out;

reg [15:0] adc_id_reg = 16'b0;
reg [15:0] counter = 0;
reg [5:0] control = 0 ;


always @(posedge clk)
begin

	adc_id_reg = {8'b00000000,adc_id[7:0]};

	if(control==60)
	begin
		out = (switch==0)? counter:adc_id_reg;
		trigger = 1'b1;
		control = 0;
		counter = counter + 1'b1;
	end
	else 
	begin
		trigger = 1'b0;
		out = 16'b0;
	end

	
	control = control + 1'b1;
end


endmodule