module Gen_CBit (

	input clk,
	input rst,
	input live,
	input load_packet,
	input get_packet,
	input [15:0] cbit_in,

	output reg [15:0] cbit_out

);


reg [7:0] local_waddr = 8'b0;
reg [7:0] local_raddr = 8'b0;


reg [3:0] load_packet_pipe = 4'b0;
reg [3:0] get_packet_pipe = 4'b0;

reg rden = 1'b0;
reg wren = 1'b0;
wire [15:0] q;


MEM_CB _ram(clk,cbit_in,local_raddr,rden,local_waddr,wren,q);


always @(posedge clk)
begin

	load_packet_pipe = load_packet_pipe << 1;
	load_packet_pipe[0] = load_packet;
	
	get_packet_pipe = get_packet_pipe << 1;
	get_packet_pipe[0] = get_packet;
	
	//if(rst | ~live)
	if(~live)
	begin
		local_waddr = 8'b0;
		local_raddr = 8'b0;
		load_packet_pipe = 4'b0;
		get_packet_pipe = 4'b0;
		cbit_out = 16'b0;
	end

		
	wren = (load_packet_pipe[0])?	1'b1 : 1'b0;		
	local_waddr = (load_packet_pipe[3])? local_waddr + 1'b1 : local_waddr;
	
	rden = (get_packet_pipe[0])? 1'b1 : 1'b0;
	local_raddr = (get_packet_pipe[3])? local_raddr + 1'b1 : local_raddr;

	cbit_out = q;
	
	
	
end

endmodule
