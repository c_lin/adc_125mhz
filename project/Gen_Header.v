module Gen_Header(
	input clk,
	input dv,
	output header
);


reg [2:0] dv_pipe;

always @(posedge clk)
begin

	dv_pipe = dv_pipe << 1;
	dv_pipe = dv;
	

end
endmodule
