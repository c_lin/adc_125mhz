module Counter_Samples(
	input clk,
	input trig,
	output reg q
);

reg [7:0] cnt = 99;

always @(posedge clk)
begin


	if(trig)  
	begin
		cnt = 0;
	end

	if( cnt < 64 )
	begin
		q = 1'b1;
		cnt = cnt + 1'b1;	
	end
	else 
	begin
		q = 1'b0;
	end

	
end

endmodule