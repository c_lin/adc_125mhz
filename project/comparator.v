module comparator(clk,pt1,ph1,pt0,ph0,pt,ph);

input wire clk;

input wire [13:0] ph0;
input wire [5:0]  pt0;

input wire [13:0] ph1;
input wire [5:0]  pt1;

output reg [13:0] ph;
output reg [5:0]  pt;



always @(posedge clk)
begin

	pt = (ph1>ph0)? pt1:pt0;
	ph = (ph1>ph0)? ph1:ph0;
end

endmodule