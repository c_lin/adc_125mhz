module Ped_Tracker (
	clk,
	data,
	start,
	ped,
	done
);

input wire clk;
input wire [13:0] data;
input wire start;

output reg [13:0] ped;
output reg done = 1'b0;

reg [13:0] ped_min;
reg ena = 1'b0;

reg [1:0] pipeline = 2'b00;
reg [12:0] control = 13'b0;
reg [12:0] stat_low = 13'b0;
reg [12:0] stat_high = 13'b0;
reg [12:0] stat_equal = 13'b0;
//reg [199:0] filter = 200'b0;
reg mode = 1'b0;

integer ptr;

always @( posedge clk )
begin

	pipeline[1] <= pipeline[0];
	pipeline[0] <= start;
	
	if( pipeline[1]==1'b1 && pipeline[0]==1'b0 )
	begin
		ena = 1'b1;
		stat_low = 13'b0;
		stat_high = 13'b0;
		stat_equal = 13'b0;
		ped_min = 14'h3fff;
		control = 13'b0;
//		filter = 200'b0;
		done = 1'b0;
		mode = 1'b0;
	end
	
	if( ena )
	begin
		case(mode)
			1'b0:
			begin
				ped_min = (data<ped_min)? data : ped_min;
				if(control==8001)
				begin
					control = 13'b0;
					mode = 1'b1;
				end
			end	
		
			1'b1:
			begin
/*			
				if( data >= (ped_min-100) && data <= (ped_min+99) )
				begin
					ptr = data-(ped_min-100);
					stat_high  = (data>ped_min  && filter[ptr]==1'b1)? stat_high + 1'b1  : stat_high; 
					stat_low   = (data<ped_min  && filter[ptr]==1'b1)? stat_low + 1'b1   : stat_low; 
					stat_equal = (data==ped_min && filter[ptr]==1'b1)? stat_equal + 1'b1 : stat_equal;
					filter[ptr] = 1'b1;
				end
*/			


				stat_high  = (data>ped_min)? stat_high + 1'b1  : stat_high; 
				stat_low   = (data<ped_min)? stat_low + 1'b1   : stat_low; 
				stat_equal = (data==ped_min)? stat_equal + 1'b1 : stat_equal;

					
				if(control==8000)
				begin
				
					if( (stat_low[12:0] + stat_equal[12:1]) > stat_high[12:0] && 
						(stat_high[12:0] + stat_equal[12:1]) > stat_low[12:0] )
					begin
						ped = ped_min;// + 100;
						ena = 1'b0;
						done = 1'b1;
//						filter = 200'b0;
					end
				
					ped_min = ( stat_high > stat_low)? ped_min + 1'b1 : ped_min;
				
//					filter = 200'b0;
					stat_low = 13'b0;
					stat_high = 13'b0;
					stat_equal = 13'b0;
					control = 13'b0;
				
				end
			end		
		endcase
		
		control = control + 1'b1;
	end
	else
	begin
		done = 1'b0;
	end	
	
end

endmodule
