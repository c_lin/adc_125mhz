module TLK_Status (
	clk,
	dv,
	err,
	q
);

input wire clk;
input wire dv;
input wire err;

output reg q = 1'b1;

always @(posedge clk)
begin

	q = (dv==1'b1)? err : q;

end
endmodule
