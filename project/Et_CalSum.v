module Et_CalSum (
	clk,
	in0,
	in1,
	lock,
	out
);

input wire clk;
input wire lock;
input wire [15:0] in0;
input wire [15:0] in1;

output reg [15:0] out;


reg [15:0] sum;
reg [9:0] pipeline_lock;

always @(posedge clk)
begin
	
	pipeline_lock <= pipeline_lock << 1;
	pipeline_lock[0] = lock;
	
	if(pipeline_lock[9]==1'b0)
	begin
		out = in0 + in1;
	end
	else
	begin
		if(in0[15]==1'b1 && in1[15]==1'b0)
		begin
			sum[14:0] = in0[14:0] + in1[14:2];
			out[15:0] = {1'b1,sum[14:0]};
		end
		else if(in0[15]==1'b0 && in1[15]==1'b1)
		begin
			sum[14:0] = in0[14:2] + in1[14:0];
			out[15:0] = {1'b1,sum[14:0]};	
		end
		else if(in0[15]==1'b1 && in1[15]==1'b1)
		begin
			sum[15:0] = in0[14:0] + in1[14:0];
			out[15:0] = {1'b1,sum[14:0]};
		end
		else if(in0[15]==1'b0 && in1[15]==1'b0)
		begin
			sum[15:0] = in0[14:0] + in1[14:0];
			if(sum[15]==1'b0)
				out[15:0] = sum[15:0];	
			else 
			begin
				sum = sum >> 2;	
				out[15:0] = {1'b1,sum[14:0]};
			end
		end
		else
		begin
			;
		end
	end	
	
end

endmodule
