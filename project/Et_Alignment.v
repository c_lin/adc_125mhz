module Et_Alignment(
	clk,
	LIVE,
	first_adc,
	data_rx,
	data_tx,
	pattern_rx,
	pattern_tx,
	maxt,

	out_tx,
	out_rx,
	lock_out,
	error,
	send_error
);

input wire clk;
input wire LIVE;
input wire first_adc;
input wire [15:0] data_rx;
input wire [15:0] data_tx;
input wire [15:0] pattern_tx;
input wire [15:0] pattern_rx;
input wire [8:0] maxt;

output reg [15:0] out_tx;
output reg [15:0] out_rx;
output reg lock_out = 1'b0;
output reg error = 1'b1;
output reg send_error = 1'b0;

reg lock = 1'b0;

wire [15:0] out_wire;
reg got_tx = 1'b0;

reg rena = 1'b0;
reg [8:0] waddr = 10;
reg [8:0] raddr = 0;
reg [8:0] raddr_start = 0;
reg [8:0] timer = 9'b0;

Et_Buffer _buffer(clk,data_tx,raddr,rena,waddr,1'b1,out_wire);


reg [15:0] pipeline_tx [2:0];
reg [15:0] pipeline_rx [5:0];
reg [9:0] pipeline_lock;

always @(posedge clk)
begin

	
	pipeline_lock = pipeline_lock << 1;
	pipeline_lock[0] = lock;
	lock_out = pipeline_lock[2];

	pipeline_tx[2] = pipeline_tx[1];
	pipeline_tx[1] = pipeline_tx[0];
	pipeline_tx[0] = data_tx;

	pipeline_rx[5] = pipeline_rx[4];
	pipeline_rx[4] = pipeline_rx[3];	
	pipeline_rx[3] = pipeline_rx[2];	
	pipeline_rx[2] = pipeline_rx[1];
	pipeline_rx[1] = pipeline_rx[0];
	pipeline_rx[0] = (first_adc==1'b0)? data_rx : 16'b0;	

	waddr = waddr + 1'b1;
	raddr = raddr + 1'b1;
	
	if(~LIVE)
	begin
		got_tx = 1'b0;
		lock = 1'b0;
		rena = 1'b0;
		timer = 9'b0;
		error = 1'b1;
	end

	if( ~lock && ~got_tx && pipeline_tx[1]==pattern_tx && pipeline_tx[0]==16'b0 && pipeline_tx[2]==16'b0)
	begin
		timer = 9'b0;
		got_tx = 1'b1;
		raddr_start = waddr - 3;
		if(first_adc)
		begin
			raddr = raddr_start;
			rena = 1'b1;
			lock = 1'b1;
			error = 1'b0;
		end
	end
	
	if( ~lock && got_tx && pipeline_rx[1]==pattern_rx && pipeline_rx[0]==16'b0 && pipeline_rx[2]==16'b0)
	begin
		raddr = raddr_start;
		rena = 1'b1;
		lock = 1'b1;
		error = 1'b0;
	end

	if( ~lock && got_tx && ~first_adc)
	begin
		if(timer>maxt)
		begin	
			lock = 1'b1;
			error = 1'b1;
		end
	
		timer = timer + 1'b1;
	end


	if(pipeline_lock[2] && ~error)
	begin
		out_tx = out_wire;
		out_rx = pipeline_rx[5];		
	end
	else
	begin
		out_tx = 16'b0;
		out_rx = 16'b0;
	end	
	
	send_error = (pipeline_lock[2]==1'b0 && pipeline_lock[1]==1'b1)? 1'b1 : 1'b0;

end


endmodule
