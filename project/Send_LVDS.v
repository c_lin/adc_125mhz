module Send_LVDS(
	clk,
	veto_crate,
	tlk_err,
	dc_err,
	send_align,
	out,
	switch
);

input wire clk;
input wire veto_crate;
input wire [1:0] tlk_err;
input wire [1:0] dc_err;
input wire send_align;
output reg out = 1'b0;
output reg switch = 1'b0;

reg [1:0] ena = 2'b00;
reg [4:0] control = 5'b00000;

always @( posedge clk)
begin

	ena = (tlk_err[1]==1'b1 && ~veto_crate)? 2'b01 : ena;
	ena = (dc_err[1]==1'b1 && ~veto_crate)? 2'b11 : ena;
	ena = (send_align==1'b1 && veto_crate)? 2'b10 : ena;
	
	if(ena==2'b01) // TLK
	begin
	
		if(control==0)
		begin
			out = 1'b1;
			switch = 1'b1;
		end
		else if(control==1)
		begin
			out = 1'b0;
			switch = 1'b1;
		end
		else if(control==2)
		begin
			out = 1'b0;
			switch = 1'b1;
		end		
		else if(control==3)
		begin
			out = tlk_err[0];
			switch = 1'b1;
			ena = 2'b00;
		end
	
		control = control + 1'b1;
		
	end
	else if(ena==2'b11) // Daisy-Chain
	begin
	
		if(control==0)
		begin
			out = 1'b1;
			switch = 1'b1;
		end
		else if(control==1)
		begin
			out = 1'b0;
			switch = 1'b1;
		end
		else if(control==2)
		begin
			out = 1'b1;
			switch = 1'b1;
		end		
		else if(control==3)
		begin
			out = dc_err[0];
			switch = 1'b1;
			ena = 2'b00;
		end
	
		control = control + 1'b1;
		
	end	
	else if(ena==2'b10) // FEFE for LVDS alignment
	begin
		
		switch = 1'b1;
		
		case(control)
			5'b00000: out = 1'b1;
			5'b00001: out = 1'b1;
			5'b00010: out = 1'b1;
			5'b00011: out = 1'b1;
			5'b00100: out = 1'b1;
			5'b00101: out = 1'b1;
			5'b00110: out = 1'b1;
			5'b00111: out = 1'b0;
			5'b01000: out = 1'b1;
			5'b01001: out = 1'b1;
			5'b01010: out = 1'b1;
			5'b01011: out = 1'b1;
			5'b01100: out = 1'b1;
			5'b01101: out = 1'b1;
			5'b01110: out = 1'b1;
			5'b01111:
				begin
					out = 1'b0;
					ena = 2'b00;
				end	
		endcase
		
		control = control + 1'b1;
	
	end
	else 
	begin
	
		out = 1'b0;
		control = 5'b00000;
		switch = 1'b0;
		
	end
end

endmodule
