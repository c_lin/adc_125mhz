module MEMO_FIFO(
	in,
	wclk,
	rclk,	
	delay,

	q
);

input wire [13:0] in;
input wire wclk;
input wire rclk;
input wire [3:0] delay;

output reg [13:0] q;


reg [4:0] raddr = 5'b0;
reg [4:0] waddr = 5'b1;

wire [13:0] q_wire;

ADC_FIFO2 _fifo(in,raddr,rclk,waddr,wclk,1'b1,q_wire);

always @(posedge rclk)
begin
	waddr = waddr + 1'b1;
	raddr = waddr - 4'b1111 - delay;
	q = q_wire;
end

endmodule
