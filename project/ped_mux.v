module ped_mux (
	clk,
	data,
	sw,
	q
);

input wire clk;
input wire [223:0] data;
input wire [3:0] sw;

output reg [13:0] q;

reg [13:0] reg00;
reg [13:0] reg01;
reg [13:0] reg02;
reg [13:0] reg03;
reg [13:0] reg04;
reg [13:0] reg05;
reg [13:0] reg06;
reg [13:0] reg07;
reg [13:0] reg08;
reg [13:0] reg09;
reg [13:0] reg10;
reg [13:0] reg11;
reg [13:0] reg12;
reg [13:0] reg13;
reg [13:0] reg14;
reg [13:0] reg15;

always @(posedge clk)
begin
	
	reg00 = data[ 13:  0];
	reg01 = data[ 27: 14];
	reg02 = data[ 41: 28];
	reg03 = data[ 55: 42];
	reg04 = data[ 69: 56];
	reg05 = data[ 83: 70];
	reg06 = data[ 97: 84];
	reg07 = data[111: 98];
	reg08 = data[125:112];
	reg09 = data[139:126];
	reg10 = data[153:140];
	reg11 = data[167:154];
	reg12 = data[181:168];
	reg13 = data[195:182];
	reg14 = data[209:196];
	reg15 = data[223:210];

	case(sw)
		4'b0000: q = reg00;
		4'b0001: q = reg01;
		4'b0010: q = reg02;
		4'b0011: q = reg03;
		4'b0100: q = reg04;
		4'b0101: q = reg05;
		4'b0110: q = reg06;
		4'b0111: q = reg07;
		4'b1000: q = reg08;
		4'b1001: q = reg09;
		4'b1010: q = reg10;
		4'b1011: q = reg11;
		4'b1100: q = reg12;
		4'b1101: q = reg13;
		4'b1110: q = reg14;
		4'b1111: q = reg15;
	endcase
	

end

endmodule
