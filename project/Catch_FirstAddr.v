module Catch_FirstAddr(
	clk,
	get_packet,
	addr_in,
	use_lossless,
	first
);


input wire clk;
input wire get_packet;
input wire [11:0] addr_in;
input wire use_lossless;


output reg first = 1'b0;

reg [11:0] addr_out = 999;
reg [4:0] pipe_get_packet;
integer ptr;

always @(posedge clk)
begin
		ptr = (use_lossless)? 2 : 4;

		pipe_get_packet = pipe_get_packet << 1;
		pipe_get_packet[0] = get_packet;

		//addr_out = (pipe_get_packet[4]==1'b1)? addr_in : addr_out;
		addr_out = (pipe_get_packet[ptr]==1'b1)? addr_in : addr_out;
		
		first = (addr_in == addr_out)? 1'b1 : 1'b0;
end

endmodule



