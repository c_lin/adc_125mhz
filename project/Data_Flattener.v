module Data_Flattener (

	input clk,
	input get_packet,
	input  [1:0] header,
	input  [223:0] data,
	
	input [28:0] timestamp,
	input [9:0]  ispill,
	input [15:0] ievt,
	input [15:0] cbit,
	input [4:0] icrate,
	input [4:0] islot,
 	
	output reg [15:0] q


);

reg [5:0] get_packet_pipe = 4'b0;


reg [28:0] timestamp_reg = 29'b0;
reg [9:0]  ispill_reg = 10'b0;
reg [15:0] ievt_reg = 16'b0;
reg [15:0] cbit_reg = 16'b0;
reg [4:0] icrate_reg = 5'b0;
reg [4:0] islot_reg = 5'b0;


reg [3:0] cnt_ch = 4'b0000;
reg [2:0] cnt_hd = 3'b000;


always @(posedge clk)
begin
	
	if(get_packet_pipe[0]) 
	begin
		icrate_reg <= icrate;
		islot_reg <= islot;
		ispill_reg <= ispill;
	end

	if(get_packet_pipe[4]) 
	begin
		timestamp_reg <= timestamp;
		ievt_reg <= ievt;
		cbit_reg <= cbit;
	end

	
end

always @(posedge clk)
begin

	get_packet_pipe = get_packet_pipe << 1;
	get_packet_pipe[0] = get_packet;

	if (header==2'b11) 
	begin
		//q = 'hC000;
		case(cnt_hd)
			3'b000 : q[15:0] = { 2'b11 , ispill_reg[3:0], islot_reg[4:0], icrate_reg[4:0]  };
			3'b001 : q[15:0] = { 2'b11 , ievt_reg[7:0], ispill_reg[9:4] };
			3'b010 : q[15:0] = { 2'b11 , timestamp_reg[5:0], ievt_reg[15:8] }; 
			3'b011 : q[15:0] = { 2'b11 , timestamp_reg[19:6] };
			3'b100 : q[15:0] = { 2'b11 , cbit_reg[4:0], timestamp_reg[28:20] };
			3'b101 : q[15:0] = { 2'b11 , 3'b000, cbit_reg[15:5] };
		endcase

		cnt_hd = cnt_hd + 1'b1;
		cnt_ch = 4'b0000;
	end
	else if (header==2'b10)
	begin
	
		case(cnt_ch)
			4'b0000 : q = {2'b10 , data[ 13:  0]};
			4'b0001 : q = {2'b10 , data[ 27: 14]};
			4'b0010 : q = {2'b10 , data[ 41: 28]};
			4'b0011 : q = {2'b10 , data[ 55: 42]};
			4'b0100 : q = {2'b10 , data[ 69: 56]};
			4'b0101 : q = {2'b10 , data[ 83: 70]};
			4'b0110 : q = {2'b10 , data[ 97: 84]};
			4'b0111 : q = {2'b10 , data[111: 98]};
			4'b1000 : q = {2'b10 , data[125:112]};
			4'b1001 : q = {2'b10 , data[139:126]};
			4'b1010 : q = {2'b10 , data[153:140]};
			4'b1011 : q = {2'b10 , data[167:154]};
			4'b1100 : q = {2'b10 , data[181:168]};
			4'b1101 : q = {2'b10 , data[195:182]};
			4'b1110 : q = {2'b10 , data[209:196]};
			4'b1111 : q = {2'b10 , data[223:210]};			
		endcase	
			
		cnt_ch = cnt_ch + 1'b1;
	end
	else if (header==2'b01)
	begin
		q = 'h4000;
		cnt_ch = 4'b0000;
		cnt_hd = 3'b000;
	end
	else
	begin
		q = 'h0000;
		cnt_ch = 4'b0000;
		cnt_hd = 3'b000;
	end
	
	
	

end
endmodule