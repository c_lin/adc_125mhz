module Self_Trigger(clk,ena,mode,trigger);

input wire clk;
input wire ena;
input wire [2:0] mode;

output reg trigger;

reg [1:0] pipeline;
reg send = 1'b0;
reg [3:0] count = 4'b0;


always @( posedge clk)
begin


	pipeline = pipeline << 1;
	pipeline[0] = ena;

	send = (pipeline[0] == 1'b1 && pipeline[1] == 1'b0)? 1'b1:send;



	if(send)
	begin
		if(mode==0) // preL1
		begin
			if(count==1) 	  trigger = 1'b1;
			else if(count==2) trigger = 1'b1;
			else if(count==3) trigger = 1'b1;
			else if(count==4) trigger = 1'b0;
			else if(count==5)
			begin
				count = 4'b0;
				trigger = 1'b0;
				send = 1'b0;
			end
		end
		else if(mode==1) // L1
		begin
			if(count==1) 	  trigger = 1'b1;
			else if(count==2) trigger = 1'b0;
			else if(count==3) trigger = 1'b0;
			else if(count==4) trigger = 1'b0;
			else if(count==5)
			begin
				count = 4'b0;
				trigger = 1'b0;
				send = 1'b0;
			end
		end	
		else if(mode==2) // L1 w PS
		begin
			if(count==1) 	  trigger = 1'b1;
			else if(count==2) trigger = 1'b1;
			else if(count==3) trigger = 1'b0;
			else if(count==4) trigger = 1'b0;
			else if(count==5)
			begin
				count = 4'b0;
				trigger = 1'b0;
				send = 1'b0;
			end
		end	
		else if(mode==3) // align
		begin
			if(count==1) 	  trigger = 1'b1;
			else if(count==2) trigger = 1'b0;
			else if(count==3) trigger = 1'b1;
			else if(count==4) trigger = 1'b0;
			else if(count==5)
			begin
				count = 4'b0;
				trigger = 1'b0;
				send = 1'b0;
			end
		end		
		else if(mode==4) // Reset
		begin
			if(count==1) 	  trigger = 1'b1;
			else if(count==2) trigger = 1'b1;
			else if(count==3) trigger = 1'b1;
			else if(count==4) trigger = 1'b1;
			else if(count==5)
			begin
				count = 4'b0;
				trigger = 1'b0;
				send = 1'b0;
			end
		end							
		
		count = count + 1'b1;
	end	
	else 
	begin
		count = 4'b0;
		trigger = 1'b0;
	end
	


end

endmodule
