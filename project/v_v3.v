module v_v3(
	clk,
	baseline,
	v,
	v3,
	out,
	dv
);

input wire clk;
input wire [13:0] baseline;
input wire [13:0] v;
input wire [13:0] v3;

output reg [15:0] out;
output reg dv;

always @(posedge clk)
begin
	
	if( v >= baseline && v3 >= baseline )
	begin
		out = v + v3 + v3 + v3;
		dv = 1'b1;
	end
	else 
	begin
		out = 16'h0000;
		dv = 1'b0;
	end
end

endmodule
