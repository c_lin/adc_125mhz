module Pedestal_Tracking(
	clk,
	rst,
	ena,
	data,
	ped
);

input wire clk;
input wire rst;
input wire ena;
input wire [13:0] data;

output reg [13:0] ped;

endmodule
