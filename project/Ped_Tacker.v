module Ped_Tacker (
	clk,
	data,
	live,
	ped
);

input wire clk;
input wire [13:0] data;
input wire live;

output reg [13:0] ped;

reg [13:0] ped_tmp;
reg ena = 1'b0;
reg lock = 1'b0;
reg find_min = 1'b0;
reg find_ped = 1'b0;

reg [1:0] pipeline = 2'b00;
reg [15:0] control;

reg [15:0] stat_low = 16'b0;
reg [15:0] stat_high = 16'b0;
reg [15:0] stat_equal = 16'b0;


always @( posedge clk )
begin

	pipeline[1] <= pipeline[0];
	pipeline[0] <= live;
	
	if( pipeline[1]==1'b1 && pipeline[0]==1'b0 )
	begin
		ena = 1'b1;
		lock = 1'b0;
		stat_low = 16'b0;
		stat_high = 16'b0;
		stat_equal = 16'b0;
		ped_tmp = 14'h3fff;
		control = 16'b0;
		find_min = 1'b1;
		find_ped = 1'b0;
	end
	
	if( ena )
	begin
		if(find_min)
		begin
			if(control>=0 && control<=5000)
			begin
				ped_tmp = (data<ped_tmp)? data : ped_tmp;
			end
			else if(control==5001)
			begin
				find_min = 1'b0;
				find_ped = 1'b1;
				control = 16'b0;
			end
		end	
		
		if(find_ped)
		begin
			stat_high = (data>ped_tmp)? stat_high + 1'b1 : stat_high; 
			stat_low = (data<ped_tmp)? stat_low + 1'b1 : stat_low; 
			stat_equal = (data==ped_tmp)? stat_equal + 1'b1 : stat_equal;
			
			if(control==5000)
			begin
				
				if( (stat_low[15:0] + stat_equal[15:1]) > stat_high[15:0] && 
					 (stat_high[15:0] + stat_equal[15:1]) > stat_low[15:0] )
				begin
					ped = ped_tmp;
					find_ped = 1'b0;
					ena = 1'b0;
				end
				
				ped_tmp = ( stat_high > stat_low)? ped_tmp + 1'b1 : ped_tmp;
				
				stat_low = 16'b0;
				stat_high = 16'b0;
				stat_equal = 16'b0;
				control = 16'b0;
			end
		end
		
		control = control + 1'b1;
	end
end

endmodule
