module MEM_Writter(

	input clk,
	input ena,
	
	output reg [11:0] waddr,
	output reg wena
	
);

always @(posedge clk)
begin

	if(ena)
	begin
		wena <= 1;
		waddr <= waddr + 1'b1;
	end
	else
	begin
		wena <= 0;
	end

end
endmodule
