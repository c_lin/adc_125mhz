module CB_taps (
	clk,
	in,
	tap00,
	tap01,
	tap02,
	tap03,
	tap04,
	tap05
);

    input clk;

    input 	[13:0] in;
    output 	[13:0] tap00, tap01, tap02, tap03, tap04, tap05; 

    reg [13:0] sr [5:0];
    integer n;

    always @(posedge clk)
    begin

		for (n = 5; n>0; n = n-1)
		begin
			sr[n] <= sr[n-1];
		end

		sr[0] <= in;

    end

    assign tap00 = sr[0];
    assign tap01 = sr[1];
    assign tap02 = sr[2];
    assign tap03 = sr[3];
    assign tap04 = sr[4];
    assign tap05 = sr[5];

	 

endmodule